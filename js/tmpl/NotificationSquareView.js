define([
    "ax/af/Container",
    "ax/ext/ui/Label",
    "ax/ext/ui/Image"
], function(
    Container,
    Label,
    Image
) {
    "use strict";

    return function() {
        return {
            klass: Container,
            css: "notificationSquareContainer",
            children: [{
                klass: Container,
                css: "profileBar backgroundColorWhite",
                children: [{
                    klass: Image,
                    id: "profileImage",
                    css: "profileImage",
                    src: ""
                },{
                    klass: Label,
                    id: "#name",
                    css: "profileName font24 colorDarkGray textOverflowEllipsis",
                    text: ""
                },{
                    klass: Image,
                    css: "adImage",
                    src: "images/ad.png"
                }]
            },{
                klass: Container,
                css: "messageBar colorDarkGray font24 backgroundColorDarkYellow",
                children: [{
                    klass: Label,
                    id:"notificationDetail",
                    text: ""
                }, {
                    klass: Image,
                    css: "notificationLogo",
                    src: "images/logoTransparent.png"
                }]
            }]
        };
    };
});