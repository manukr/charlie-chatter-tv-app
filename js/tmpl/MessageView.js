define([
    "ax/af/Container",
    "ax/ext/ui/Label",
    "ax/ext/ui/Image"
], function(
    Container,
    Label,
    Image
) {
    "use strict";

    return function() {
        return {
            klass: Container,
            css: "messageContainer",
            children: [{
                klass: Container,
                css: "profileBar",
                children: [{
                    klass: Container,
                    css: "profileName font24 colorDarkGray backgroundColorWhite",
                    children: [{
                        klass: Label,
                        id: "#name",
                        css: "profileNameText textOverflowEllipsis",
                        text: ""
                    }]
                },{
                    klass: Container,
                    css: "profileImageContainer",
                    children: [{
                        klass: Image,
                        id: "profileImage",
                        css: "profileImage",
                        src: ""
                    }]
                }]
            },{
                klass: Container,
                css: "messageBar backgroundColorDarkYellow colorDarkGray font24",
                children: [{
                    klass: Label,
                    id:"messageDetail",
                    text: ""
                }]
            }]
        };
    };
});