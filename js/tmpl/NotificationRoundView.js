define([
    "ax/af/Container",
    "ax/ext/ui/Label",
    "ax/ext/ui/Image"
], function(
    Container,
    Label,
    Image
) {
    "use strict";

    return function() {
        return {
            klass: Container,
            css: "notificationContainer",
            children: [{
                klass: Container,
                css: "backgroundColorDarkYellowSemiTransparent",
                children: [{
                    klass: Container,
                    css: "notificationImage backgroundColorWhite",
                    children: [{
                        klass: Image,
                        id: "notificationImage",
                        src: ""
                    }]
                }, {
                    klass: Container,
                    css: "colorDarkGray font24 notificationMessage",
                    children: [{
                        klass: Label,
                        id: "notificationMessage",
                        css: "notificationMessageText",
                        text: ""
                    }]
                }]
            }]
        };
    };
});