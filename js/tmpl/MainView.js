define([
    "ax/af/Container",
    "model/Message"
], function(
    Container,
    Message
) {
    "use strict";

    return function() {
        return {
            klass: Container,
            focusable: true,
            children: [{
                klass: Container,
                id: "#rightPanel",
                css: "rightPanel"
            },{
                klass: Container,
                id: "#leftPanel",
                css: "leftPanel"
            }]
        };
    };
});