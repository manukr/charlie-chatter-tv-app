/**
 * Configuration file for ITNS. Will store api end point, multiscreen channel name etc.
 *
 * @author Joyce Wang <joyce.wang@accedo.tv>
 */
define([], function() {
    return {
        version: "1.0.1", // app version, app version should only be [0-9].[0-9].[0-9], due to the unperfect checking in build.gradle

        channelName: "com.accedo.itns", // Multiscreen channel name
        tvName: "ITNS-TV", // tv name
        multiscreenEvents: {
            clientConnect: "clientConnect",
            clientDisconnect: "clientDisconnect",
            message: "message"
        },
        messageNumber: 2, // the maximum messages showing on screen
        templateID: {
            chatMessage: 1, // itns.ui_template_id, agree with android device, 1 = chat message, 2 = notification
            notificationRound: 2,
            notificationSquare: 3
        }
    };
});