define([
    "ax/class",
    "ax/af/mvc/Controller",
    "ax/af/mvc/view",
    "../tmpl/MainView",
    "ax/af/mediator",
    "app.config",
    "model/Message",
    "model/NotificationRound",
    "model/NotificationSquare",
    "ax/console",
    "ax/util",
    "ax/device/vKey",
    "ax/af/evt/type",
    "ax/af/focusManager",
    "ax/device",
    "ax/device/interface/System"
], function (
    klass,
    Controller,
    view,
    MainView,
    mediator,
    config,
    Message,
    NotificationRound,
    NotificationSquare,
    console,
    util,
    vKey,
    evtType,
    focusManager,
    device,
    ISystem
) {
    "use strict";

    return klass.create(Controller, {
        /**
         * Show <MAX_DISPLAY_MESSAGE> number of messages and notifications in screen, the old ones will be removed.
         */
        MAX_DISPLAY_MESSAGE: 2
    }, {
        init: function () {
            this.setView(view.render(MainView));
        },

        setup: function () {
            // set up when the controller opens

            // Init variables
            this.rightPanelMessages = []; // Array of model/Message or model/NotificationRound
            this.leftPanelMessages = []; // Array of model/NotificationSquare
            this.rightPanel = this.getView().find("rightPanel");
            this.leftPanel = this.getView().find("leftPanel");

            // Init mediator
//            mediator.subscribe(config.multiscreenEvents.message, util.bind(function(data) {
//                var jsonData = JSON.parse(data);
//                if (jsonData.messageData && jsonData.messageData.itns) {
//                    if (jsonData.messageData.itns.ui_template_id === config.templateID.chatMessage) {
//                        // Receive a chat message
//                        this.addNewItemToRightPanel(new Message(jsonData.messageData.itns.data));
//                    }
//                    else if (jsonData.messageData.itns.ui_template_id === config.templateID.notificationRound) {
//                        // Receive a notification
//                        this.addNewItemToRightPanel(new NotificationRound(jsonData.messageData.itns.data));
//                    }
//                    else if (jsonData.messageData.itns.ui_template_id === config.templateID.notificationSquare) {
//                        this.addNewItemToLeftPanel(new NotificationSquare(jsonData.messageData.itns.data));
//                    }
//                    else {
//                        console.log("[MainController]: receive an unknown message, with ui_template_id = " + jsonData.messageData.itns.ui_template_id);
//                    }
//
//                }
//                else {
//                    console.log("[MainController]: receive a message event with unexpected format.");
//                }
//            }, this));
            mediator.subscribe(config.multiscreenEvents.message, util.bind(function(data) {
                var jsonData = JSON.parse(data);
                if (jsonData.messageData && jsonData.messageData.itns) {
                    if (jsonData.messageData.itns.ui_template_id === config.templateID.chatMessage) {
                    	
                    	//we need to messag authentication here
                    	var stateShow = true;
                    	var token=jsonData.messageData.itns.user_token;
                    	var message_id=jsonData.messageData.itns.message_id;
                    	var message_token=jsonData.messageData.itns.message_token;
                    	var send_by=jsonData.messageData.itns.send_by;
                    	var send_to=jsonData.messageData.itns.send_to;
                    	var json={
                    			"token" :token,
                    	        "message_token": message_token,
                    	        "send_to": send_to,
                    	        "message_id": message_id,
                    	        "send_by": send_by,
                    	        "flow_type" : "tv_receive" 
                    	};
//                    	this.addNewItemToRightPanel(new Message(jsonData.messageData.itns.data));
                        
                    	console.log("[Message Notification]: Just received the message, Now Authenticating");
//                     NEW CODE
                       var client = new XMLHttpRequest();
                       client.open("post", "http://52.63.211.171/api/get_auth_message?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0OTU0OTczMzMsInN1YiI6MTEyLCJpc3MiOiJodHRwOlwvXC81Mi42My4yMTEuMTcxXC9hcGlcL3JlZ2lzdGVyIiwiaWF0IjoxNDYzOTYxMzMzLCJuYmYiOjE0NjM5NjEzMzMsImp0aSI6Ijg1NGYzNDA2ZjBjMGIzZTBhNDg3MjgwZDQ0ZTVhMWZhIn0.63k8eWa4E37BPbFCexZ3QH69trVdni4O5Dh8VcwFmY0", true);
                       client.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                       client.send(json);  /* Send to server */ 
                       console.log("[Message Notification]: Just completed the message, Now Authenticating");
                       console.log(this);
                        /* Check the response status */  
                        client.onloadend = function() 
                        {
                        	console.log("[State] : "+client.readyState);
                        	if (client.readyState == 4 && client.status == 200) 
                           {
                              stateShow = true;
                             
                           }else{
                        	   
                        	  stateShow = false;
                           }
                        	
                        }
                        console.log(stateShow);
                        if (stateShow) {
                        	this.addNewItemToRightPanel(new Message(jsonData.messageData.itns.data));
						}
                        
                        
                    }
                    else if (jsonData.messageData.itns.ui_template_id === config.templateID.notificationRound) {
                        // Receive a notification
                        this.addNewItemToRightPanel(new NotificationRound(jsonData.messageData.itns.data));
                    }
                    else if (jsonData.messageData.itns.ui_template_id === config.templateID.notificationSquare) {
                        this.addNewItemToLeftPanel(new NotificationSquare(jsonData.messageData.itns.data));
                    }
                    else {
                        console.log("[MainController]: receive an unknown message, with ui_template_id = " + jsonData.messageData.itns.ui_template_id);
                    }

                }
                else {
                    console.log("[MainController]: receive a message event with unexpected format.");
                }
            }, this));


            mediator.subscribe(config.multiscreenEvents.clientConnect, util.bind(function() {
                console.log("[MainController]: receive a 'clientConnect' message");
                var data = {
                    imageUrl: "images/logoBlackLarge.png",
                    text: "Connected to a mobile device successfully."
                };
                this.addNewItemToRightPanel(new NotificationRound(data));
            }, this));

            mediator.subscribe(config.multiscreenEvents.clientDisconnect, util.bind(function() {
                console.log("[MainController]: receive a 'clientDisconnect' message");
                var data = {
                    imageUrl: "images/logoBlackLarge.png",
                    text: "The mobile device is disconnected."
                };
                this.addNewItemToRightPanel(new NotificationRound(data));
            }, this));

            // Add Event Listners
            this.addEventListeners();

            focusManager.focus(this.getView());
        },

        addEventListeners: function () {
            // Check network status
            device.system.addEventListener(ISystem.EVT_NETWORK_STATUS_CHANGED, function(newStatus) {
                console.log("[MainController]: network connection status changed: " + newStatus);
                device.system.exit({
                    toTV: true
                });
            });
        },

        removeEventListeners: function() {
            device.system.removeEventListener(ISystem.EVT_NETWORK_STATUS_CHANGED);
        },

        /**
         * Add new item to right panel, the item could be Message or NotificationRound
         *
         * @param {model/Message | model/NotificationRound | model/NotificationSquare} item
         */
        addNewItemToRightPanel: function(item) {
            this.rightPanelMessages.push(item);
            this.rightPanel.attach(item);

            if (this.rightPanelMessages.length > this.constructor.MAX_DISPLAY_MESSAGE) {
                this.rightPanel.detach(this.rightPanelMessages.shift());
            }
        },

        /**
         * Add new item to left panel, the item is NotificationSquare
         *
         * @param {model/NotificationSquare} item
         */
        addNewItemToLeftPanel: function(item) {
            this.leftPanelMessages.push(item);
            this.leftPanel.attach(item);

            if (this.leftPanelMessages.length > this.constructor.MAX_DISPLAY_MESSAGE) {
                this.leftPanel.detach(this.leftPanelMessages.shift());
            }
        },

        reset: function () {
            // clean up when the controller gets replaced/closed
            this.removeEventListeners();

            this.rightPanelMessages = [];
            this.leftPanelMessages = [];
            this.rightPanel = null;
            this.leftPanel = null;
        }
    });
});