define([
    "ax/class",
    "ax/af/mvc/view",
    "../tmpl/NotificationRoundView",
    "ax/af/Container",
    "ax/util",
    "ax/console"
], function(
    klass,
    view,
    NotificationView,
    Container,
    util,
    console
) {
    "use strict";

    return klass.create(Container, {
        /**
         * notification will fade out after <FADE_OUT_TIME> seconds
         */
        FADE_OUT_TIME: 10
    }, {
        init: function(data) {
            this._super();
            this.attach(view.render(NotificationView));

            // Update content from data
            this.find("notificationMessage").setText(data.text || "");
            this.find("notificationImage").setSrc(data.imageUrl).then(null, util.bind(function() {
                console.log("[NotificationRound]: load profile image fails.");
                this.find("notificationImage").setSrc("images/logoBlackLarge.png");
            }, this));

            // Message fade out
            util.delay(this.constructor.FADE_OUT_TIME).then(util.bind(function() {
                this.addClass("fadeOut");
            }, this));
        }
    });
});