/**
 * Channel model to implement Tizen Multiscreen SDK 2.0.
 * Will require app.config.channelName and app.config.tvName
 *
 * @example
 * channel.init().then(function() {
 *      // add event listeners
 *      channel.addEventListener("hello");
 *      // when channel get a "hello" event, will publish a exact same event to mediator
 *      mediator.subscribe("hello", function(event, data) {
 *          // event handling
 *      });
 * });
 *
 * @author Joyce Wang <joyce.wang@accedo.tv>
 */
define([
    "ax/console",
    "ax/promise",
    "ax/af/mediator",
    "ax/device",
    "app.config",
    "ax/util"
], function(
    console,
    promise,
    mediator,
    device,
    config,
    util
) {
    // "use strict"; Disable "use strict" in this model, as it will cause "Attempt to assign to readonly property" issue.

    return {
        channel: null,

        init: function() {
            var deferred = promise.defer();

            if (device.platform === "tizen" && typeof window.msf !== "undefined") {
                window.msf.local(util.bind(function(error, service) {
                    if (error !== null) {
                        console.log("[Channel]: msf.local error: " + error);
                        deferred.reject(error);
                        return;
                    }

                    // Create a reference to a communication "channel"
                    this.channel = service.channel(config.channelName);
                    console.log("[Channel]: channel created successfully");

                    // Connect to the channel
                    this.channel.connect({name: config.tvName}, function(error) {
                        if (error) {
                            console.log("[Channel]: channel connect error: " + error);
                            deferred.reject(error);
                            return;
                        }

                        console.log("[Channel]: You are connected to channel!");
                        deferred.resolve();
                    });
                }, this));
            }
            else {
                deferred.reject("no window.msf found!");
            }

            return deferred.promise;
        },

        addEventListener: function(eventName) {
            try {
                this.channel.on(eventName, function(msg) {
                    console.log("[Channel]: receive '" + eventName + "' event: " + msg);
                    // Send this event to app
                    mediator.publish(eventName, msg);
                });
                console.log("[Channel]: channel add event listener: " + eventName);
            } catch(e) {
                console.log("[Channel]: add event listener " + eventName + " fail: " + e);
            }
        },

        fireEvent: function(eventName, msg) {
            try {
                this.channel.publish(eventName, msg);
                console.log("[Channel]: fire event " + eventName + ": " + msg + " to channel.");
            } catch(e) {
                console.log("[Channel]: fire event " + eventName + " fail: " + e);
            }
        }
    };
});