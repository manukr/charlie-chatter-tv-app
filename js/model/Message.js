define([
    "ax/class",
    "ax/af/mvc/view",
    "../tmpl/MessageView",
    "ax/af/Container",
    "ax/util",
    "ax/console"
], function(
    klass,
    view,
    MessageView,
    Container,
    util,
    console
) {
    "use strict";

    return klass.create(Container, {
        /**
         * message will fade out after <FADE_OUT_TIME> seconds
         */
        FADE_OUT_TIME: 10
    }, {
        init: function(data) {
            this._super();
            this.attach(view.render(MessageView));

            // Update content from data
            this.find("name").setText((data.firstName || "") + " " + (data.lastName || ""));
            this.find("messageDetail").setText(data.text || "");
            this.find("profileImage").setSrc(data.imageUrl).then(null, util.bind(function() {
                console.log("[Message]: load profile image fails.");
                this.find("profileImage").deinit();
            }, this));

            // Message fade out
            util.delay(this.constructor.FADE_OUT_TIME).then(util.bind(function() {
                this.addClass("fadeOut");
            }, this));
        }
    });
});