define([
    "ax/class",
    "ax/af/mvc/view",
    "../tmpl/NotificationSquareView",
    "ax/af/Container",
    "ax/util",
    "ax/console"
], function(
    klass,
    view,
    NotificationView,
    Container,
    util,
    console
) {
    "use strict";

    return klass.create(Container, {
        /**
         * notification will fade out after <FADE_OUT_TIME> seconds
         */
        FADE_OUT_TIME: 10
    }, {
        init: function(data) {
            this._super();
            this.attach(view.render(NotificationView));

            // Update content from data
            this.find("name").setText((data.firstName || "") + " " + (data.lastName || ""));
            this.find("notificationDetail").setText(data.text || "");
            this.find("profileImage").setSrc(data.imageUrl).then(null, util.bind(function() {
                console.log("[NotificationSquare]: load profile image fails.");
                this.find("profileImage").setSrc("images/defaultProfile.png");
            }, this));

            // Message fade out
            util.delay(this.constructor.FADE_OUT_TIME).then(util.bind(function() {
                this.addClass("fadeOut");
            }, this));
        }
    });
});