//to load the xdk core
require([
    "ax/Env",
    "ax/af/mvc/AppRoot",
    "ctrl/MainController",
    "ax/console",
    "model/Channel",
    "app.config",
    "ax/device/vKey",
    "ax/device",
    "css!../css/style"
], function(
    Env,
    AppRoot,
    MainCtrl,
    console,
    Channel,
    config,
    vKey,
    device
) {
    "use strict";

    var sEnv = Env.singleton(),
        sAppRoot = AppRoot.singleton();

    console.log("[app]: App version v" + config.version + " --- All module loaded!");
    sEnv.addEventListener(sEnv.EVT_ONLOAD, function() {

        sAppRoot.setMainController(MainCtrl).then(function() {

            // MainCtrl has been opened
            Channel.init().then(function() {
                console.log("[app]: channel init success");
                Channel.addEventListener(config.multiscreenEvents.clientConnect);
                Channel.addEventListener(config.multiscreenEvents.clientDisconnect);
                Channel.addEventListener(config.multiscreenEvents.message);
            }, function(error) {
                console.log("[app]: channel connect fail: " + error);
            }).done();

            // Press exit to quit app
            sEnv.addEventListener(sEnv.EVT_ONKEY, function(keyEvent) {
                if (keyEvent.id === vKey.EXIT.id) {
                    console.log("[app]: exit key pressed");
                    device.system.exit({
                        toTV: true
                    });
                }
            });

        }).done();
    });
});